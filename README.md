# Argo Tunnel client

Contains the command-line client and its libraries for Argo Tunnel, a tunneling daemon that proxies any local webserver through the Cloudflare network.

## Usage

To run the container:

````shell
podman run -e CF_API_TOKEN=your-token \
    -e CF_SUBDOMAIN=test \
    -e CF_DOMAIN=example.com \
    -e CF_ZONE_ID=your-zone-id \
    registry.gitlab.com/uni-corn.me/cloudflared/argo-free-tunnel \
    tunnel --url http://127.0.0.1:81
````

The API Token must be granted `Zone.DNS` edit permission.

## Getting started

    go install github.com/cloudflare/cloudflared/cmd/cloudflared

User documentation for Argo Tunnel can be found at https://developers.cloudflare.com/argo-tunnel/
